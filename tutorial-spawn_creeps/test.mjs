import { SpawnsBuilder } from "../support/SpawnsBuilder.mjs"
import { FlagsBuilder } from "../support/FlagsBuilder.mjs"
import { getObjectsByPrototype } from "../game/utils.mjs"
import { cleanGame } from "../support/cleanGame.mjs"
import { Creep } from "../game/prototypes.mjs"
import { loop } from "./main.mjs"
import { expect } from "chai"

describe("Tutorial 7 Spawn Creeps", () => {
  it("creates creeps that can move", () => {
    cleanGame()
    const aFlag = new FlagsBuilder().at("aFlagPosition").build()
    const anotherFlag = new FlagsBuilder().at("anotherFlagPosition").build()
    const spawn = new SpawnsBuilder().at("spawnPosition").build()
    let creeps = getObjectsByPrototype(Creep)
    expect(creeps.length).to.eq(0)

    loop()

    creeps = getObjectsByPrototype(Creep)
    expect(creeps.length).to.eq(1)
    expect(creeps[0].isIn(spawn)).to.eq(false)
    expect(creeps[0].isIn(aFlag)).to.eq(true)

    loop()

    creeps = getObjectsByPrototype(Creep)
    expect(creeps.length).to.eq(2)
    expect(creeps[1].isIn(spawn)).to.eq(false)
    expect(creeps[1].isIn(anotherFlag)).to.eq(true)
  })
})
