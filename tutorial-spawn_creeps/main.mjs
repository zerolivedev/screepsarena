import { MOVE } from "../game/constants.mjs";
import { Flag, StructureSpawn } from "../game/prototypes.mjs";
import { getObjectsByPrototype } from "../game/utils.mjs";

const creeps = []
export function loop() {
    const flags = getObjectsByPrototype(Flag)
    const spawns = getObjectsByPrototype(StructureSpawn)
    const mySpawn = spawns[0]
    const creep = mySpawn.spawnCreep([MOVE]).object
    if(creep) { creeps.push(creep) }

    if(creeps[0]) {
        creeps[0].moveTo(flags[0])
    }

    if(creeps[1]) {
        creeps[1].moveTo(flags[1])
    }
}
