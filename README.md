# Screeps Arena code

## System requirements

- Docker version 20.10.18 or compatible
- Docker Compose version v2.3.3 or compatible

## How to run the tests

- First of all, you have to execute `sh scripts/up.sh` for prepare the project.
- Once is prepared. You can execute `sh scripts/test.sh` for run the tests.

> When you finish your work session I recommend that you use `sh scripts/down.sh` ^^
