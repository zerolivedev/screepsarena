FROM node:16.17.1-slim
WORKDIR /opt/screeps

COPY package* .

RUN npm install

COPY . .
