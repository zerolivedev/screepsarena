# TO DO

- [ ] Add creeps bodies parts in old tests
- [ ] Remember that the bodies parts are objects that add hits to the creep
- [ ] Check how many hits gives every body part
- [ ] Calculate damage for a creep
- [ ] Implement CreepStub#findClosestByPath
- [ ] Implement real Position
