import { FlagStub } from "../game/FlagStub.mjs"
import { Position } from "../game/Position.mjs"

export const FlagsBuilder = class {
  constructor() {
    this.descriptor = {
      position: new Position("flagPosition")
    }
  }
  at(name) {
    this.descriptor.position = new Position(name)

    return this
  }

  build() {
    if (!global.collections.flags) {
      global.collections.flags = []
    }

    const newFlag = new FlagStub(this.descriptor)
    global.collections.flags.push(newFlag)
    return newFlag
  }
}
