import { ATTACK, HEAL, RANGED_ATTACK, MOVE } from "../game/constants.mjs"
import { CreepStub } from "../game/CreepStub.mjs"
import { Position } from "../game/Position.mjs"

export const CreepsBuilder = class {
  constructor() {
    this.descriptor = {
      position: new Position("creepPosition"),
      my: true,
      body: []
    }
  }

  with() {
    return this
  }

  for() {
    return this
  }

  my() {
    this.descriptor.my = true

    return this
  }

  enemy() {
    this.descriptor.my = false

    return this
  }

  at(position) {
    this.descriptor.position = new Position(position)

    return this
  }

  heal(quantity) {
    const bodyPart = { type: HEAL }

    this._addBodyPart(bodyPart, quantity)

    return this
  }

  attack(quantity) {
    const bodyPart = { type: ATTACK }

    this._addBodyPart(bodyPart, quantity)

    return this
  }

  rangedAttack(quantity) {
    const bodyPart = { type: RANGED_ATTACK }

    this._addBodyPart(bodyPart, quantity)

    return this
  }

  move(quantity) {
    const bodyPart = { type: MOVE }

    this._addBodyPart(bodyPart, quantity)

    return this
  }

  build() {
    if (!global.collections.creeps) {
      global.collections.creeps = []
    }

    const newCreep = new CreepStub(this.descriptor)
    global.collections.creeps.push(newCreep)
    return newCreep
  }

  _addBodyPart(bodyPart, quantity) {
    for(let times = 0; times < quantity; times ++) {
      this.descriptor.body.push(bodyPart)
    }
  }
}
