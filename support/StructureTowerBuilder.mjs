import { Position } from "../game/Position.mjs"
import { StructureTowerStub } from "../game/StructureTowerStub.mjs"

export const StructureTowerBuilder = class {
  constructor() {
    this.descriptor = {
      my: true,
      position: new Position("structureTowerPosition")
    }
  }

  for() {
    return this
  }

  my() {
    this.descriptor.my = true

    return this
  }

  build() {
    if (!global.collections.structureTower) {
      global.collections.structureTower = []
    }

    const newTower = new StructureTowerStub(this.descriptor)
    global.collections.structureTower.push(newTower)
    return newTower
  }
}
