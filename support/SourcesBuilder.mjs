import { SourceStub } from "../game/SourceStub.mjs"
import { Position } from "../game/Position.mjs"

export const SourcesBuilder = class {
  descriptor = {
    position: new Position("sourcePosition")
  }

  build = () => {
    if (!global.collections.source) {
      global.collections.source = []
    }

    const newSource = new SourceStub(this.descriptor)
    global.collections.source.push(newSource)
    return newSource
  }
}
