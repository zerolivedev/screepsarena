import { StructureSpawnStub } from "../game/StructureSpawnStub.mjs"
import { Position } from "../game/Position.mjs"

export const SpawnsBuilder = class {
  constructor() {
    this.descriptor = {
      position: new Position('spawnPosition')
    }
  }

  at(name) {
    this.descriptor.position = new Position(name)

    return this
  }

  build() {
    if (!global.collections.structureSpawn) {
      global.collections.structureSpawn = []
    }

    const newSpawn = new StructureSpawnStub(this.descriptor)
    global.collections.structureSpawn.push(newSpawn)
    return newSpawn
  }
}
