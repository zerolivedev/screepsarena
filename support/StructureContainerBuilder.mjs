import { Position } from "../game/Position.mjs"
import { StructureContainerStub } from "../game/StructureContainerStub.mjs"

export const StructureContainerBuilder = class {
  constructor() {
    this.descriptor = {
      position: new Position("structureContainerPosition")
    }
  }

  build() {
    if (!global.collections.structureContainer) {
      global.collections.structureContainer = []
    }

    const newContainer = new StructureContainerStub(this.descriptor)
    global.collections.structureContainer.push(newContainer)
    return newContainer
  }
}
