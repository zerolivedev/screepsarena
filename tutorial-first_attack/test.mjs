import { expect } from "chai"
import { cleanGame } from "../support/cleanGame.mjs"
import { loop } from "./main.mjs"
import { getObjectsByPrototype } from "../game/utils.mjs"
import { Creep } from "../game/prototypes.mjs"
import { CreepsBuilder } from "../support/CreepsBuilder.mjs"

describe("Tutorial 3 First attack", () => {
  it("kills the enemy creep", () => {
    cleanGame()
    let myCreep = new CreepsBuilder().for().my().at("myPosition").build()
    let enemyCreep = new CreepsBuilder().for().enemy().at("enemyPosition").build()
    expect(myCreep.currentPosition()).not.to.eq(enemyCreep.currentPosition())
    expect(enemyCreep.hasBeenAttacked()).to.eq(false)

    loop()

    enemyCreep = retrieveUpdatedEnemyCreep()
    myCreep = retrieveUpdatedMyCreep()
    expect(myCreep.currentPosition()).to.eq(enemyCreep.currentPosition())
    expect(enemyCreep.hasBeenAttacked()).to.eq(false)

    loop()

    enemyCreep = retrieveUpdatedEnemyCreep()
    expect(enemyCreep.hasBeenAttacked()).to.eq(true)
  })

  function retrieveUpdatedEnemyCreep() {
    const enemyCreep = getObjectsByPrototype(Creep).find(creep => !creep.my)

    return enemyCreep
  }

  function retrieveUpdatedMyCreep() {
    const myCreep = getObjectsByPrototype(Creep).find(creep => creep.my)

    return myCreep
  }
})
