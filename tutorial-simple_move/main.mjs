import { getObjectsByPrototype } from '../game/utils.mjs'
import { Creep, Flag } from '../game/prototypes.mjs'

export function loop() {
    var creeps = getObjectsByPrototype(Creep)
    var flags = getObjectsByPrototype(Flag)

    creeps[0].moveTo(flags[0])
}
