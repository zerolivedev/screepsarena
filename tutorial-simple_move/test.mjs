import { cleanGame } from "../support/cleanGame.mjs"
import { CreepsBuilder } from "../support/CreepsBuilder.mjs"
import { FlagsBuilder } from "../support/FlagsBuilder.mjs"
import { loop } from "./main.mjs"
import { expect } from "chai"

describe("Tutorial 2 Simple move", () => {
  it("captures the flag", () => {
    cleanGame()
    let creep = new CreepsBuilder().for().my().at("myPosition").build()
    let flag = new FlagsBuilder().at("anotherPosition").build()
    expect(creep.isIn(flag)).to.eq(false)

    loop()

    expect(creep.isIn(flag)).to.eq(true)
  })
})
