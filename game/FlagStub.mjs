
export const FlagStub = class {
  constructor({ position }) {
    this.position = position
  }

  currentPosition() {
    return this.position
  }
}
