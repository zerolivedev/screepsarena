import { RESOURCE_ENERGY } from "./constants.mjs"

export const StructureTowerStub = class {
  constructor({ my, position }) {
    this.my = my
    this.position = position

    this.store = {
      [RESOURCE_ENERGY]: 0
    }
  }

  attack(target) {
    target.retrieveAttack()
  }

  addToStore(quantity, resource) {
    this.store[resource] += quantity
  }

  currentPosition() {
    return this.position
  }

  hasBeenTransferEnergy() {
    return (this.store[RESOURCE_ENERGY] > 0)
  }
}
