
export const Position = class {
  constructor(name) {
    this.name = name
  }

  current() {
    return this.name
  }

  isEqualTo(anotherPosition) {
    return (this.name === anotherPosition.name)
  }
}
