import { RESOURCE_ENERGY } from "./constants.mjs"

export const SourceStub = class {
  MAX_ENERGY = 1000
  constructor({ position }) {
    this.position = position
    this.store = {
      [RESOURCE_ENERGY]: this.MAX_ENERGY
    }
  }

  retrieve(resource, quantity) {
    let quantityToRetrieve = 0

    if (this.store[resource] < quantity) {
      if (this.store[resource] > 0) {
        quantityToRetrieve = this.store[resource]

        this.store[resource] = 0
      }
    } else {
      this.store[resource] -= quantity

      quantityToRetrieve = quantity
    }

    return quantityToRetrieve
  }

  currentPosition = () => {
    return this.position
  }

  hasFullEnergy() {
    return (this.store[RESOURCE_ENERGY] === this.MAX_ENERGY)
  }
}
