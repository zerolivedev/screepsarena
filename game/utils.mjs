import { Creep, Flag, StructureTower, StructureContainer, StructureSpawn, Source } from "./prototypes.mjs"

function getTicks() {
  return 1
}

function getObjectsByPrototype(prototype) {
  const collections = {
    [Creep]: global.collections.creeps,
    [Flag]: global.collections.flags,
    [StructureTower]: global.collections.structureTower,
    [StructureContainer]: global.collections.structureContainer,
    [StructureSpawn]: global.collections.structureSpawn,
    [Source]: global.collections.source
  }

  return collections[prototype] || []
}

export { getTicks, getObjectsByPrototype }
