import { ERR_NOT_IN_RANGE, RESOURCE_ENERGY } from "./constants.mjs"

export const CreepStub = class {
  WITHDRAW_QUANTITY = 10
  HITS_MAX = 10
  constructor({ my, position, body }) {
    this.position = position
    this.my = my
    this.retrievedDamage = false
    this.body = body
    this._hasBeenHealed = false
    this.hits = this.HITS_MAX
    this.hitsMax = this.HITS_MAX
    this.store = {
      [RESOURCE_ENERGY]: 0,
      getFreeCapacity: (resource) => {
        return (250 - this.store[resource])
      }
    }
  }

  retrieveAttack() {
    this.hits --
    this.retrievedDamage = true
  }

  retrieveHeal() {
    this.retrievedDamage = false
  }

  attack(target) {
    if (!this._inRange(target)) { return ERR_NOT_IN_RANGE }

    target.retrieveAttack()
  }

  rangedAttack(target) {
    target.retrieveAttack()
  }

  heal(target) {
    if (!this._inRange(target)) { return ERR_NOT_IN_RANGE }

    this._hasBeenHealed = true
  }

  moveTo(destination) {
    this.position = destination.currentPosition()
  }

  withdraw(target, resource) {
    if (!this._inRange(target)) { return ERR_NOT_IN_RANGE }

    const quantity = target.retrieve(resource, this.WITHDRAW_QUANTITY)

    this.store[resource] += quantity
  }

  transfer(target, resource) {
    if (!this._inRange(target)) { return ERR_NOT_IN_RANGE }
    const quantity = 10

    this.store[resource] -= quantity

    target.addToStore(quantity, resource)
  }

  harvest(target) {
    return this.withdraw(target, RESOURCE_ENERGY)
  }

  findClosestByPath(targets) {
    return targets[0]
  }

  currentPosition() {
    return this.position
  }

  hasBeenAttacked() {
    return this.retrievedDamage
  }

  hasBeenHealed() {
    return this._hasBeenHealed
  }

  hasEnergy() {
    return (this.store[RESOURCE_ENERGY] > 0)
  }

  isIn(target) {
    const anotherPosition = target.currentPosition()

    return this.position.isEqualTo(anotherPosition)
  }

  _inRange(target) {
    return (this.currentPosition() === target.currentPosition())
  }
}
