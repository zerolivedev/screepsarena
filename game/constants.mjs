
const ERR_NOT_IN_RANGE = -9
const RESOURCE_ENERGY = "resource_energy"
const ATTACK = "attack"
const RANGED_ATTACK = "range_attack"
const HEAL = "heal"
const MOVE = "move"

export { ERR_NOT_IN_RANGE, ATTACK, RANGED_ATTACK, HEAL, MOVE, RESOURCE_ENERGY }
