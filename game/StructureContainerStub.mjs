import { RESOURCE_ENERGY } from "./constants.mjs"

export const StructureContainerStub = class {
  MAX_ENERGY = 100
  constructor({ position }) {
    this.position = position
    this.store = {
      [RESOURCE_ENERGY]: this.MAX_ENERGY
    }
  }

  retrieve(resource) {
    const quantity = 10

    this.store[resource] -= quantity

    return quantity
  }

  currentPosition() {
    return this.position
  }

  hasBeenWithdrawEnergy() {
    return this.store[RESOURCE_ENERGY] != this.MAX_ENERGY
  }
}
