const Creep = "creep"
const Flag = "flag"
const StructureContainer = "structureContainer"
const StructureTower = "structureTower"
const StructureSpawn = "structureSpawn"
const Source = "source"

export { Creep, Flag, StructureContainer, StructureTower, StructureSpawn, Source }
