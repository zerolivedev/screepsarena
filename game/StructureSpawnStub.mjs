import { CreepsBuilder } from "../support/CreepsBuilder.mjs"
import { RESOURCE_ENERGY } from "./constants.mjs"

export const StructureSpawnStub = class {
  constructor({ position }) {
    this.position = position
    this.store = {
      [RESOURCE_ENERGY]: 0
    }
  }

  addToStore(quantity, resource) {
    this.store[resource] += quantity
  }

  spawnCreep(_bodyParts) {
    return {
      object: new CreepsBuilder().with().move(1).at(this.position.name).build()
    }
  }

  currentPosition() {
    return this.position
  }

  hasEnergy() {
    return (this.store[RESOURCE_ENERGY] > 0)
  }
}
