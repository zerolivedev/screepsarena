import { getObjectsByPrototype } from "../game/utils.mjs";
import { Creep, Source, StructureSpawn } from "../game/prototypes.mjs";
import { ERR_NOT_IN_RANGE, RESOURCE_ENERGY } from "../game/constants.mjs";

export function loop() {
    const creeps = getObjectsByPrototype(Creep)
    const sources = getObjectsByPrototype(Source)
    const spawns = getObjectsByPrototype(StructureSpawn)

    const myCreep = creeps[0]
    const mySpawn = spawns[0]
    const aSource = sources[0]

    if(myCreep.store.getFreeCapacity(RESOURCE_ENERGY)) {
        if(myCreep.harvest(aSource) === ERR_NOT_IN_RANGE) {
            myCreep.moveTo(aSource)
        }
    } else {
        if(myCreep.transfer(mySpawn, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
            myCreep.moveTo(mySpawn)
        }
    }
}
