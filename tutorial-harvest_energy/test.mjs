import { SourcesBuilder } from "../support/SourcesBuilder.mjs"
import { CreepsBuilder } from "../support/CreepsBuilder.mjs"
import { SpawnsBuilder } from "../support/SpawnsBuilder.mjs"
import { cleanGame } from "../support/cleanGame.mjs"
import { loop } from "./main.mjs"
import { expect } from "chai"

describe("Tutorial 8 Harvest Energy", () => {
  it("harvest a source for fill the spawn's energy", () => {
    cleanGame()
    const creep = new CreepsBuilder().build()
    const source = new SourcesBuilder().build()
    const spawn = new SpawnsBuilder().build()
    expect(creep.isIn(source)).to.eq(false)
    expect(creep.isIn(spawn)).to.eq(false)
    expect(source.hasFullEnergy()).to.eq(true)
    expect(spawn.hasEnergy()).to.eq(false)

    loop()

    expect(creep.isIn(source)).to.eq(true)
    expect(creep.isIn(spawn)).to.eq(false)
    expect(source.hasFullEnergy()).to.eq(true)

    executeLoopUntilCreepIsFullOfEnergy()

    expect(creep.isIn(source)).to.eq(true)
    expect(creep.isIn(spawn)).to.eq(false)
    expect(source.hasFullEnergy()).to.eq(false)

    loop()

    expect(creep.isIn(source)).to.eq(false)
    expect(creep.isIn(spawn)).to.eq(true)
    expect(spawn.hasEnergy()).to.eq(false)

    loop()

    expect(creep.isIn(source)).to.eq(false)
    expect(creep.isIn(spawn)).to.eq(true)
    expect(spawn.hasEnergy()).to.eq(true)
  })

  function executeLoopUntilCreepIsFullOfEnergy() {
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
    loop()
  }
})
