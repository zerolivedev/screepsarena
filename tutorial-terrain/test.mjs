import { loop } from "./main.mjs"
import { expect } from "chai"
import { CreepsBuilder } from "../support/CreepsBuilder.mjs"
import { FlagsBuilder } from "../support/FlagsBuilder.mjs"
import { cleanGame } from "../support/cleanGame.mjs"

describe("Tutorial 6 Terrain", () => {
  it("move a creep to the closer flag", () => {
    cleanGame()
    const creep = new CreepsBuilder().build()
    const closesFlag = new FlagsBuilder().at('closesPosition').build()
    const fartherFlag = new FlagsBuilder().at('fartherPosition').build()
    expect(creep.isIn(closesFlag)).to.eq(false)
    expect(creep.isIn(fartherFlag)).to.eq(false)

    loop()

    expect(creep.isIn(fartherFlag)).to.eq(false)
    expect(creep.isIn(closesFlag)).to.eq(true)
  })
})
