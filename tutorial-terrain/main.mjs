import { getObjectsByPrototype } from "../game/utils.mjs";
import { Creep, Flag } from "../game/prototypes.mjs";

export function loop() {
    const creeps = getObjectsByPrototype(Creep)
    const flags = getObjectsByPrototype(Flag)

    for(let creep of creeps) {
        const closestFlag = creep.findClosestByPath(flags)

        creep.moveTo(closestFlag)
    }
}
