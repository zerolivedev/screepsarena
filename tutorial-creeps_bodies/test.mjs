import { expect } from "chai"
import { cleanGame } from "../support/cleanGame.mjs"
import { loop } from "./main.mjs"
import { CreepsBuilder } from "../support/CreepsBuilder.mjs"

describe("Tutorial 4 Creeps Bodies", () => {
  it("kills the enemy creep", () => {
    cleanGame()
    const rangedCreepInitialPosition = "myPosition"
    let enemyCreep = new CreepsBuilder().for().enemy().with().heal(1).with().rangedAttack(11).with().move(12).at("enemyPosition").build()
    let myMeleeCreep = new CreepsBuilder().for().my().at("myPosition").with().attack(1).with().move(1).build()
    let myHealerCreep = new CreepsBuilder().for().my().at("myAnotherPosition").with().heal(1).with().move(1).build()
    let myRangedCreep = new CreepsBuilder().for().my().at(rangedCreepInitialPosition).with().rangedAttack(1).with().move(1).build()
    expect(myMeleeCreep.isIn(enemyCreep)).to.eq(false)
    expect(myRangedCreep.isIn(enemyCreep)).to.eq(false)
    expect(myHealerCreep.isIn(myMeleeCreep)).to.eq(false)
    expect(enemyCreep.hasBeenAttacked()).to.eq(false)
    doDamageTo(myMeleeCreep)

    loop()

    expect(myMeleeCreep.isIn(enemyCreep)).to.eq(true)
    expect(myHealerCreep.isIn(myMeleeCreep)).to.eq(true)
    expect(enemyCreep.hasBeenAttacked()).to.eq(true)
    expect(myHealerCreep.hasBeenHealed()).to.eq(false)

    loop()

    expect(myHealerCreep.hasBeenHealed()).to.eq(true)
  })

  function doDamageTo(target) {
    target.retrieveAttack()
  }
})
