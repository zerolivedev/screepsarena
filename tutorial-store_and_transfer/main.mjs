import { getObjectsByPrototype } from '../game/utils.mjs';
import { ERR_NOT_IN_RANGE, RESOURCE_ENERGY } from '../game/constants.mjs';
import { Creep, StructureContainer, StructureTower } from '../game/prototypes.mjs';

export function loop() {
    const creeps = getObjectsByPrototype(Creep)
    const enemyCreeps = creeps.filter(creep => !creep.my)
    const enemyCreep = enemyCreeps[0]
    const myCreeps = creeps.filter(creep => creep.my)
    const creep = myCreeps[0]
    const towers = getObjectsByPrototype(StructureTower)
    const myTowers = towers.filter(tower => tower.my)
    const tower = myTowers[0]
    const container = getObjectsByPrototype(StructureContainer)[0]

    if(creep.store[RESOURCE_ENERGY] === 0) {
        if (creep.withdraw(container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
            creep.moveTo(container)
        }
    } else {
        if (creep.transfer(tower, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
            creep.moveTo(tower)
        }
    }

    tower.attack(enemyCreep)
}
