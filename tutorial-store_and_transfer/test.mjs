import { StructureContainerBuilder } from "../support/StructureContainerBuilder.mjs"
import { StructureTowerBuilder } from "../support/StructureTowerBuilder.mjs"
import { CreepsBuilder } from "../support/CreepsBuilder.mjs"
import { cleanGame } from "../support/cleanGame.mjs"
import { loop } from "./main.mjs"
import { expect } from "chai"

describe("Tutorial 5 Store and transfer", () => {
  let enemyCreep
  let container
  let tower
  let myCreep

  beforeEach(() => {
    cleanGame()
    enemyCreep = new CreepsBuilder().for().enemy().build()
    tower = new StructureTowerBuilder().for().my().build()
    container = new StructureContainerBuilder().build()
    myCreep = new CreepsBuilder().for().my().at("myPosition").build()
  })

  it("attacks enemy creep with the tower", () => {

    loop()

    expect(enemyCreep.hasBeenAttacked()).to.eq(true)
  })

  it("withdraws energy to store in the tower", () => {
    expect(myCreep.isIn(container)).to.eq(false)
    expect(myCreep.isIn(tower)).to.eq(false)
    expect(container.hasBeenWithdrawEnergy()).to.eq(false)
    expect(tower.hasBeenTransferEnergy()).to.eq(false)
    expect(myCreep.hasEnergy()).to.eq(false)

    loop()

    expect(myCreep.isIn(container)).to.eq(true)
    expect(myCreep.hasEnergy()).to.eq(false)
    expect(container.hasBeenWithdrawEnergy()).to.eq(false)
    expect(tower.hasBeenTransferEnergy()).to.eq(false)

    loop()

    expect(myCreep.isIn(container)).to.eq(true)
    expect(myCreep.hasEnergy()).to.eq(true)
    expect(container.hasBeenWithdrawEnergy()).to.eq(true)
    expect(tower.hasBeenTransferEnergy()).to.eq(false)

    loop()

    expect(myCreep.isIn(tower)).to.eq(true)
    expect(myCreep.hasEnergy()).to.eq(true)
    expect(tower.hasBeenTransferEnergy()).to.eq(false)

    loop()

    expect(myCreep.isIn(tower)).to.eq(true)
    expect(tower.hasBeenTransferEnergy()).to.eq(true)
    expect(enemyCreep.hasEnergy()).to.eq(false)
  })
})
