import { expect } from "chai"
import { loop } from "./main.mjs"

describe("Tutorial 1 Loop and import", () => {
  it("executes the loop", () => {

    const execution = () => { loop() }

    expect(execution).not.to.throw()
  })
})
